# [Good First Bug]()

### <College Name>
#### 30.08.2023

-----

![Free Software Movement Karnataka](img/fsmk.png)

---

<!-- .slide: data-visibility="hidden" -->

![Foss Cell CMRIT](img/foss-cell.png)

-----

# Introduction

---

## Session

- Beginner <!-- .element: class="fragment" data-fragment-index="1" -->
- Hands On <!-- .element: class="fragment" data-fragment-index="2" -->
- Git + Git Platforms <!-- .element: class="fragment" data-fragment-index="3" -->
- Contributing to Public Projects <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Follow Along

<a href="https://live-presentation.fsmk.org">
live-presentation.fsmk.org
</a>

-----

![](img/git.png)

---

## What is Git ?

- distributed version control software <!-- .element: class="fragment" data-fragment-index="1" -->

---

## Why do you need Git ? <!-- .element: class="fragment" data-fragment-index="1" -->
or <!-- .element: class="fragment" data-fragment-index="2" -->
## What can you do with Git ? <!-- .element: class="fragment" data-fragment-index="2" -->

Snapshots Across Time <!-- .element: class="fragment" data-fragment-index="3" -->

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/N_7mRGuBEJY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

---

## Who created Git ?

- Linus Torvalds, circa 2005 <!-- .element: class="fragment" data-fragment-index="1" -->
- Actively developed and maintained by volunteers

---

## Who uses Git ?

- Anybody can use git <!-- .element: class="fragment" data-fragment-index="1" -->
- But importantly, it is used by most of the Free and Open Source community <!-- .element: class="fragment" data-fragment-index="2" -->
- Git helps democratise code <!-- .element: class="fragment" data-fragment-index="3" -->

-----

# Git Installation

---

- Linux 

[ Debian/Ubuntu/Debian-derivatives ] <!-- .element: style="opacity:50%; font-size:75%" -->

```
sudo apt install git-all
```

<br>

- Windows


[gitforwindows.org](https://gitforwindows.org)  <!-- .element: style="font-size:75%" -->

<br>

- M	ac

```
git --version
```

-----

# Remote Setup

---

Git is **distributed**

Git allows for multiple users to edit and modify versions, concurrently over a network <!-- .element: class="fragment" data-fragment-index="1" style="font-size:70%" -->

Git supports remotes <!-- .element: class="fragment" data-fragment-index="2" style="font-size:70%" -->

---

## Git as a Service

- Github <!-- .element: style="opacity:100%" -->
- Gitlab <!-- .element: style="opacity:100%" -->
- GitTea <!-- .element: style="opacity:70%" -->
- SourceHut <!-- .element: style="opacity:55%" -->
- And Many More . . .  <!-- .element: style="opacity:40%" -->

---

Let us setup accounts !

---

<a href="https://github.com/">

![Github](img/github.png)

</a>

---

<a href="https://gitlab.com/">

![Gitlab](img/gitlab.png)

</a>

-----

# Local Setup

---

## Testing Git

```bash
git --help
```

---

## Initial git configuration

```[1-2|4-5|7-8|10-11]
#Telling Git your name
git config --global user.name "Your Name !"

#Telling Git your email
git config --global user.email "your-email@addre.ss"

#Showing the basic configurations
git config --list
```

-----

# Local Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Repositories

---

## What are Repositories ?

- environments <!-- .element: class="fragment" data-fragment-index="1" -->
- projects <!-- .element: class="fragment" data-fragment-index="2" -->
- what git deals with <!-- .element: class="fragment" data-fragment-index="3" -->

---

You can think of a repository as a 'collection of source code files' that can be hosted and collaboratively edited.

---

They are made of 3 separate 'layers'

![](img/repo.png)

---

Two ways of getting a repository :
- create your own <!-- .element: class="fragment" data-fragment-index="1" -->
- use existing <!-- .element: class="fragment" data-fragment-index="2" -->

-----

We will look into how to take an existing project on the internet and make our modifications to this

---

There are many ways of contributing using the source code :
- Documenting, formatting and making the source code more readable <!-- .element: class="fragment" data-fragment-index="1" -->
- Maintaining projects to ensure they work with the latest versions of their dependencies <!-- .element: class="fragment" data-fragment-index="2" -->
- Investigating issues and fixing bugs <!-- .element: class="fragment" data-fragment-index="3" -->
- Contributing new features <!-- .element: class="fragment" data-fragment-index="4" -->

-----

# Remote <!-- .element: style="opacity:60%; font-size:120%" -->

# Forking a repository

---

Public projects need write permissions to edit

Forking a project is to create a copy that *you* can own and edit

---

<div class="r-stack">

![](img/fork1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/fork2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/fork3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/fork4.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/fork5.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>

---

Here is the project that we will be working with in this session.

- [Sample Project](https://github.com/fsmk/git-sample-project-0)


---

[Click here to fork the project](https://github.com/fsmk/git-sample-project-0/fork)

---

The issues page contains a list of issues that have been identified and are currently open.

We will be solving 2 of the 3 issues on this program. <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

---

<a href="https://github.com/fsmk/git-sample-project-0/issues">Issues</a>

-----

## Getting your repository 
## [ using existing ] <!-- .element: style="opacity:50%; font-size:125%" -->

```bash [1-2|4-5|7-8|10-11]
#Cloning your repository
git clone gitlab.com/<username>/git-sample-project-0

#Navigate
cd git-sample-project-0

#Checking current status
git status
```

---

####


-----

# Remote Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Pushing Code

---

Pushing code is how you "upload"

Synchronizing from local to remote <!-- .element: class="fragment current-visible" data-fragment-index="4" -->


---

```bash
# Pushing present repo to remote
git push
```

---

<div class="r-stack">

![](img/push1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/push2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/push3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/push4.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

</div>


---

Pull ?

The exact reverse. <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

Synchronizing from remote to local. <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

---

<code class="fragment current-visible" data-fragment-index="1">
git push
 </code>

<div class="r-stack">

![](img/pushpull1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/pushpull2.gif) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/pushpull3.gif) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

<code class="fragment current-visible" data-fragment-index="2">
git pull
 </code>


---

![](img/local_process.gif)

---

![](img/remote_repo.gif)

-----


# Remote Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Pull Requests/ Merge Requests


---

Pull/Merge Requests allow you to submit the changes in your fork back to the actual project. <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

Means of contributing code to actual projects. <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

<br>

Note : This is not a core Git feature, it is instead a feature of the Git platform <!-- .element: style="opacity:30%; font-size:80%" -->

---

<div class="r-stack">

![](img/fork5.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/pr1.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/pr2.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

-----

## Bugathon

---


Explore repositories that you can publicly contribute to :

<a href="https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/tree/main/2?ref_type=heads">
Guide
</a>

---

Conclusion

---

Thank you !

-----

Feedback


# [Good First Bug]()

### Christ University
#### 30.08.2023

-----

![Free Software Movement Karnataka](img/fsmk.png)

---

<!-- .slide: data-visibility="hidden" -->

![Foss Cell CMRIT](img/foss-cell.png)

-----

# Introduction

---

## Session

- Beginner <!-- .element: class="fragment" data-fragment-index="1" -->
- Hands On <!-- .element: class="fragment" data-fragment-index="2" -->
- Git + Git Platforms <!-- .element: class="fragment" data-fragment-index="3" -->
- Contributing to Public Projects <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Follow Along

<a href="https://live-presentation.fsmk.org">
live-presentation.fsmk.org
</a>

-----

![](img/git.png)

---

## What is Git ?

- distributed version control software <!-- .element: class="fragment" data-fragment-index="1" -->

---

## Why do you need Git ? <!-- .element: class="fragment" data-fragment-index="1" -->
or <!-- .element: class="fragment" data-fragment-index="2" -->
## What can you do with Git ? <!-- .element: class="fragment" data-fragment-index="2" -->

Snapshots Across Time <!-- .element: class="fragment" data-fragment-index="3" -->

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/N_7mRGuBEJY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

---

## Who created Git ?

- Linus Torvalds, circa 2005 <!-- .element: class="fragment" data-fragment-index="1" -->

---

## Who uses Git ?

- Anybody can use git <!-- .element: class="fragment" data-fragment-index="1" -->
- But importantly, it is used by most of the Free and Open Source community <!-- .element: class="fragment" data-fragment-index="2" -->
- Git helps democratise code <!-- .element: class="fragment" data-fragment-index="3" -->

-----

# Git Installation

---

- Linux 

[ Debian/Ubuntu/Debian-derivatives ] <!-- .element: style="opacity:50%; font-size:75%" -->

```
sudo apt install git-all
```

<br>

- Windows


[gitforwindows.org](https://gitforwindows.org)  <!-- .element: style="font-size:75%" -->

<br>

- Mac

```
git --version
```

-----

# Remote Setup

---

Git is **distributed**

Git allows for multiple users to edit and modify versions, concurrently over a network <!-- .element: class="fragment" data-fragment-index="1" style="font-size:70%" -->

Git supports remotes <!-- .element: class="fragment" data-fragment-index="2" style="font-size:70%" -->

---

## Git as a Service

- Github <!-- .element: style="opacity:100%" -->
- Gitlab <!-- .element: style="opacity:100%" -->
- GitTea <!-- .element: style="opacity:70%" -->
- SourceHut <!-- .element: style="opacity:55%" -->
- And Many More . . .  <!-- .element: style="opacity:40%" -->

---

Let us setup accounts !

---

<a href="https://github.com/">

![Github](img/github.png)

</a>

---

<a href="https://gitlab.com/">

![Gitlab](img/gitlab.png)

</a>

-----

# Local Setup

---

## Testing Git

```bash
git --help
```

---

## Initial git configuration

```[1-2|4-5|7-8|10-11]
#Telling Git your name
git config --global user.name "Your Name !"

#Telling Git your email
git config --global user.email "your-email@addre.ss"

#Showing the basic configurations
git config --list
```

-----

# Local Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Repositories

---

## What are Repositories ?

- environments <!-- .element: class="fragment" data-fragment-index="1" -->
- projects <!-- .element: class="fragment" data-fragment-index="2" -->
- what git deals with <!-- .element: class="fragment" data-fragment-index="3" -->

---

You can think of a repository as a 'collection of source code files' that can be hosted and collaboratively edited.

---

They are made of 3 separate 'layers'

![](img/repo.png)

---

Two ways of getting a repository :
- create your own <!-- .element: class="fragment" data-fragment-index="1" -->
- use existing <!-- .element: class="fragment" data-fragment-index="2" -->

---

## Getting your repository 
## [ using existing ] <!-- .element: style="opacity:50%; font-size:125%" -->

```bash [1-2|4-5|7-8|10-11]
#Cloning your repository
git clone gitlab.com/<username>/first-repo

#Navigate
cd first-repo

#Checking current status
git status
```

---

## Creating Files

```bash [1-2|4-5|7-8|10-11|13-14|16-17|19-20]
#Create file called 'a'
echo "A" > a

#Create file called 'b'
echo "B" > b

#Create file called 'c'
echo "C" > c

#Checking status
git status
```

---

<div class="r-stack">

![](img/stateA-0.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateA-1.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateA-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/stateA-3.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/stateA-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>


---

## Staging

```bash [1,2|1,3|5-6|8-9|5-6]
#Add file 
git add a
git add b

#Check Status
git status

#Remove File
git rm --cached b
```

---

<div class="r-stack">

![](img/stateB-0.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateB-1.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateB-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/stateB-3.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/stateB-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>


---

## Add all to staging

```bash
#Adding files manually can take time
#Sometimes you want to add the whole repo to staging

git add --all

#or

git add .

```

---

<div class="r-stack">

![](img/stateB-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateB-5.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateB-6.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

-----

# Local Workflow <!-- .element: style="opacity:60%; font-size:120%" -->

# Commits

---

## Commits

- Savepoints <!-- .element: class="fragment" data-fragment-index="1" -->
- Snaphots <!-- .element: class="fragment" data-fragment-index="2" -->
- Each commit == different verion <!-- .element: class="fragment" data-fragment-index="3" -->

---

![](img/savegame.gif) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

---

## Creating a Commit

```sh [1-2|4-5|1-2|7-8]
#Lets recheck git status
git status

#Creating a commit
git commit -m "1 - Start"

#Lets check log
git log

```

---

<div class="r-stack">

![](img/stateB-6.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateC-0.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateC-1.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

---

## Making Modification

```sh [1-2|4-5|7-8|1-2|10-11|1-2]
#Rechecking status
git status

#Modifying a
echo "X" > a

#Modifying b
echo "Y" > b

#Add a
git add a

```

---

<div class="r-stack">

![](img/stateC-1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateC-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateC-3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/stateC-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/stateC-5.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>

---

## Files in a repository

Tracked files are in any 1 of 3 states at any given time :
- modified <!-- .element: class="fragment" data-fragment-index="1" -->
- staged <!-- .element: class="fragment" data-fragment-index="2" -->
- committed <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Lets make a few more commits

Second commit

```sh [1-2|4-5|7-8|4-5|10-11|4-5]
# Removing C
rm C

# Rechecking status
git status

# Adding all to staging
git add .

# Making commit
git commit -m "2 - Removed C"
```

---

<div class="r-stack">

![](img/stateC-5.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateD-1.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateD-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/stateD-3.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/stateD-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>

---

Third commit

```sh [1-2|4-5|7-8|4-5|10-11|4-5]
# Creating ab
echo "Z" > ab

# Rechecking status
git status

# Adding all to staging
git add .

# Making commit
git commit -m "3 - Created ab"
```

---

<div class="r-stack">

![](img/stateD-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/stateE-1.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/stateE-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/stateE-3.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/stateE-4.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>

---

## Typical local workflow

![](img/local_process.gif)

---

![](img/local_repo.gif)

---

```sh
#Visualizing the current commit history

git log --graph
```

---

![](img/unbranched-3.png)

---

# Moving across commits

---

![](img/commit-hop2.gif)

```bash [1-2|4-5|1-2|7-8|1-2]
#Check Log
git log --graph

#Move to previous commit
git checkout <hash>

#Move back to latest commit
git checkout main
```
-----

# Remote Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Pushing Code

---

Pushing code is how you "upload"

Synchronizing from local to remote <!-- .element: class="fragment current-visible" data-fragment-index="4" -->


---

```bash
# Pushing present repo to remote
git push
```

---

<div class="r-stack">

![](img/push1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/push2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/push3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/push4.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

</div>


---

Pull ?

The exact reverse. <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

Synchronizing from remote to local. <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

---

<code class="fragment current-visible" data-fragment-index="1">
git push
 </code>

<div class="r-stack">

![](img/pushpull1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/pushpull2.gif) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/pushpull3.gif) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

<code class="fragment current-visible" data-fragment-index="2">
git pull
 </code>


---

![](img/local_process.gif)

---

![](img/remote_repo.gif)

-----

# Local Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Branches

---

## What are branches ?

- series of commits <!-- .element: class="fragment" data-fragment-index="1" -->
-commits that diverge from a common origin <!-- .element: class="fragment" data-fragment-index="2" -->

---
<div class="r-stack">

![](img/unbranched-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/branched-2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

</div>

---

## Creating Branches

```sh [1-2|4-5|1-2]
#List Branches
git branch -a 

#Create Branch
git branch new
```

---

<div class="r-stack">

![](img/createbranch1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/createbranch2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

</div>

---

## Switching Branches

```bash[5-6]
# Switching syntax : 
# git switch <branch name>
# git checkout <branch name>

# Switching to the branch we just created
git switch new
```

---

Lets make some changes and commit to new branch

```sh [1-2|4-5|7-8|4-5|10-11|4-5]
# Creating C again
echo "C" > c

# Rechecking status
git status

# Adding all to staging
git add .

# Making commit
git commit -m "n0"
```

---

<div class="r-stack">

![](img/createbranch1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/createbranch2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/createbranch3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/createbranch4.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

</div>

---

## Merging Branches

```bash [1-2|4-5|7-8|10-11|13-14]
# Switching back to main
git switch main

# Creating file 'd'
echo "D" > d

# Adding d
git add .; 

# Committing 
git commit -m "4"

#Merging new branch with main
git merge main -m "5"
```

---

<div class="r-stack">

![](img/createbranch4.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/createbranch5.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/createbranch6.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/createbranch7.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

</div>

-----

<a href="https://gitlab.com/fsmk/fsmk-projects/good-first-bug/-/tree/main/2?ref_type=heads">
Guide
</a>

-----

# Remote <!-- .element: style="opacity:60%; font-size:120%" -->

# Forking a repository

---

Public projects need write permissions to edit

Forking a project is to create a copy that *you* can own and edit

---

<div class="r-stack">

![](img/fork1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/fork2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/fork3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

![](img/fork4.png) <!-- .element: class="fragment current-visible" data-fragment-index="3" -->

![](img/fork5.png) <!-- .element: class="fragment current-visible" data-fragment-index="4" -->

</div>

-----

# Remote Workflow <!-- .element: style="opacity:60%; font-size:120%" -->
# Pull Requests/ Merge Requests


---

Pull/Merge Requests allow you to submit the changes in your fork back to the actual project. <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

Means of contributing code to actual projects. <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

<br>

Note : This is not a core Git feature, it is instead a feature of the Git platform <!-- .element: style="opacity:30%; font-size:80%" -->

---

<div class="r-stack">

![](img/fork5.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/pr1.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/pr2.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

-----

## Bugathon

---

Explore repositories that you can publicly contribute to

---

Conclusion

---

Thank you !

-----

Feedback

